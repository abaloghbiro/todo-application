package hu.braininghub.bh06.todo.service;

import org.junit.Before;
import org.junit.Test;

import hu.braininghub.bh06.service.exception.InvalidCredentialsException;
import hu.braininghub.bh06.todo.dao.UserDAO;
import hu.braininghub.bh06.todo.dao.impl.DefaultUserDAO;
import hu.braininghub.bh06.todo.model.User;

public class UserServiceTest {

	private UserService service;

	@Before
	public void init() {
		UserDAO dao = new DefaultUserDAO();

		User u = new User();
		u.setActive(Boolean.TRUE);
		u.setUsername("attila");
		u.setPassword("alma");
		dao.save(u);

		service = new DefaultUserService(dao);
	}

	@Test
	public void testLoginWorksProperly() throws InvalidCredentialsException {

		User u = service.login("attila", "alma");

		org.junit.Assert.assertNotNull(u);

	}

	@Test(expected = InvalidCredentialsException.class)
	public void testLoginWithInvalidCredentials() throws InvalidCredentialsException {

		service.login("attila", "alma2");
	}
}
