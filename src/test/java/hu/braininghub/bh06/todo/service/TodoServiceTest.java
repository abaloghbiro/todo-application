package hu.braininghub.bh06.todo.service;

import java.time.LocalDateTime;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import hu.braininghub.bh06.todo.dao.TodoDAO;
import hu.braininghub.bh06.todo.dao.TodoEntryDAO;
import hu.braininghub.bh06.todo.dao.impl.DefaultTodoDAO;
import hu.braininghub.bh06.todo.dao.impl.DefaultTodoEntryDAO;
import hu.braininghub.bh06.todo.model.Todo;
import hu.braininghub.bh06.todo.model.TodoEntry;

public class TodoServiceTest {

	private TodoService service;

	@Before
	public void init() {

		TodoDAO t = new DefaultTodoDAO();
		TodoEntryDAO e = new DefaultTodoEntryDAO();
		service = new DefaultTodoService(t, e);
	}

	@Test
	public void testSaveTodo() {

		Todo t = new Todo();
		t.setTodoName("testSaveTodo");
		t.setValidUntil(LocalDateTime.of(2018, 12, 31, 12, 00));

		TodoEntry entry = new TodoEntry();
		entry.setParent(t);
		entry.setValue("testTask");

		t.getEntries().add(entry);

		service.createTodo(t);

		List<Todo> todos = service.getTodos();
		org.junit.Assert.assertNotNull(todos);
		org.junit.Assert.assertEquals(1, todos.size());

		Todo loaded = todos.get(0);

		org.junit.Assert.assertNotNull(loaded.getId());
		org.junit.Assert.assertEquals(Boolean.TRUE, loaded.isActive());

		List<TodoEntry> entries = service.getTodoEntriesOfTodo(loaded.getId());
		org.junit.Assert.assertNotNull(entries);
		org.junit.Assert.assertEquals(1, entries.size());
		org.junit.Assert.assertNotNull(entries.get(0).getId());
		org.junit.Assert.assertEquals(Boolean.TRUE, entries.get(0).isActive());
		org.junit.Assert.assertEquals(loaded, entries.get(0).getParent());

	}
}
