package hu.braininghub.bh06.service.exception;

public class InvalidCredentialsException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public InvalidCredentialsException() {
		super("Username or password was incorrect!");
	}
}
