package hu.braininghub.bh06.todo.model;

public enum TodoEntryState {

	CREATED(100), IN_PROGESS(200), DONE(300), CANCELED(400);

	private Integer code;

	private TodoEntryState(Integer code) {
		this.code = code;
	}

	public Integer getCode() {
		return code;
	}
}
