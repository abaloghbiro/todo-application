package hu.braininghub.bh06.todo.model;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class Todo extends BusinessObject{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String todoName;
	private LocalDateTime validUntil;
	private List<TodoEntry> entries = new ArrayList<>();

	public String getTodoName() {
		return todoName;
	}

	public void setTodoName(String todoName) {
		this.todoName = todoName;
	}

	public List<TodoEntry> getEntries() {
		return entries;
	}

	public void setEntries(List<TodoEntry> entries) {
		this.entries = entries;
	}

	public LocalDateTime getValidUntil() {
		return validUntil;
	}

	public void setValidUntil(LocalDateTime validUntil) {
		this.validUntil = validUntil;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((entries == null) ? 0 : entries.hashCode());
		result = prime * result + ((todoName == null) ? 0 : todoName.hashCode());
		result = prime * result + ((validUntil == null) ? 0 : validUntil.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Todo other = (Todo) obj;
		if (entries == null) {
			if (other.entries != null)
				return false;
		} else if (!entries.equals(other.entries))
			return false;
		if (todoName == null) {
			if (other.todoName != null)
				return false;
		} else if (!todoName.equals(other.todoName))
			return false;
		if (validUntil == null) {
			if (other.validUntil != null)
				return false;
		} else if (!validUntil.equals(other.validUntil))
			return false;
		return true;
	}
}
