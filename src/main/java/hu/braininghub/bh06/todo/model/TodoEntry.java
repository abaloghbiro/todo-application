package hu.braininghub.bh06.todo.model;

public class TodoEntry extends BusinessObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String value;
	private Todo parent;
	private TodoEntryState currentState;

	public TodoEntry() {
		this.currentState = TodoEntryState.CREATED;
	}

	public Todo getParent() {
		return parent;
	}

	public void setParent(Todo parent) {
		this.parent = parent;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public TodoEntryState getCurrentState() {
		return currentState;
	}

	public void setCurrentState(TodoEntryState currentState) {
		this.currentState = currentState;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((currentState == null) ? 0 : currentState.hashCode());
		result = prime * result + ((parent == null) ? 0 : parent.hashCode());
		result = prime * result + ((value == null) ? 0 : value.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		TodoEntry other = (TodoEntry) obj;
		if (currentState != other.currentState)
			return false;
		if (parent == null) {
			if (other.parent != null)
				return false;
		} else if (!parent.equals(other.parent))
			return false;
		if (value == null) {
			if (other.value != null)
				return false;
		} else if (!value.equals(other.value))
			return false;
		return true;
	}

}
