package hu.braininghub.bh06.todo.service;

import hu.braininghub.bh06.service.exception.InvalidCredentialsException;
import hu.braininghub.bh06.todo.dao.UserDAO;
import hu.braininghub.bh06.todo.model.User;

public class DefaultUserService implements UserService {

	private UserDAO dao;

	public DefaultUserService(UserDAO userDAO) {
		this.dao = userDAO;
	}


	@Override
	public User login(String username, String password) throws InvalidCredentialsException {

		User user = dao.getUserByName(username);

		if (!user.getPassword().equals(password)) {
			throw new InvalidCredentialsException();
		}

		return user;
	}

}
