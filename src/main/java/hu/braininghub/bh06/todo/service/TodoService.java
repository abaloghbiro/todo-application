package hu.braininghub.bh06.todo.service;

import java.util.List;

import hu.braininghub.bh06.todo.model.Todo;
import hu.braininghub.bh06.todo.model.TodoEntry;

public interface TodoService {

	void createTodo(Todo obj);

	void deleteTodo(String id);

	void updateTodoEntry(TodoEntry entry);

	void updateTodo(Todo todo);

	void deleteTodoEntry(String todoEntryId);

	void deleteAllTodoEntry(String todoId);

	List<Todo> getTodos();

	List<TodoEntry> getTodoEntriesOfTodo(String todoId);
}
