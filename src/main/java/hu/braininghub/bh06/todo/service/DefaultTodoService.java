package hu.braininghub.bh06.todo.service;

import java.util.List;
import java.util.stream.Collectors;

import hu.braininghub.bh06.todo.dao.TodoDAO;
import hu.braininghub.bh06.todo.dao.TodoEntryDAO;
import hu.braininghub.bh06.todo.model.Todo;
import hu.braininghub.bh06.todo.model.TodoEntry;

public class DefaultTodoService implements TodoService {

	private TodoDAO todoDAO;

	private TodoEntryDAO entryDAO;

	public DefaultTodoService(TodoDAO todoDAO, TodoEntryDAO entryDAO) {
		this.todoDAO = todoDAO;
		this.entryDAO = entryDAO;
	}

	@Override
	public void createTodo(Todo obj) {

		for (TodoEntry e : obj.getEntries()) {
			entryDAO.save(e);
		}
		
		todoDAO.save(obj);

	}

	@Override
	public void deleteTodo(String id) {

		todoDAO.delete(id);
	}

	@Override
	public void updateTodoEntry(TodoEntry entry) {

		entryDAO.update(entry);

	}

	@Override
	public void updateTodo(Todo todo) {
		todoDAO.update(todo);
	}

	@Override
	public void deleteTodoEntry(String todoEntryId) {

		entryDAO.delete(todoEntryId);

	}

	@Override
	public void deleteAllTodoEntry(String todoId) {

		entryDAO.getEntities().stream().filter(e -> e.getParent().getId().equals(todoId)).collect(Collectors.toList())
				.forEach(e -> entryDAO.delete(e.getId()));

	}

	@Override
	public List<Todo> getTodos() {
		return todoDAO.getEntities();
	}

	@Override
	public List<TodoEntry> getTodoEntriesOfTodo(String todoId) {

		return todoDAO.getById(todoId).getEntries();
	}

}
