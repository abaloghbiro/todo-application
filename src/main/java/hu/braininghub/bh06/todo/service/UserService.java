package hu.braininghub.bh06.todo.service;

import hu.braininghub.bh06.service.exception.InvalidCredentialsException;
import hu.braininghub.bh06.todo.model.User;

public interface UserService {

	User login(String username, String password) throws InvalidCredentialsException;
}
