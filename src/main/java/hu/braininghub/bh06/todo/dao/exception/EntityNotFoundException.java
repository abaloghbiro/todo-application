package hu.braininghub.bh06.todo.dao.exception;

public class EntityNotFoundException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public EntityNotFoundException(String id) {
		super("Entity not found: " + id);
	}

}
