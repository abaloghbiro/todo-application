package hu.braininghub.bh06.todo.dao;

import java.util.List;

import hu.braininghub.bh06.todo.model.BusinessObject;

public interface BaseDAO<T extends BusinessObject> {

	void save(T object);

	void update(T object);

	void delete(String id);

	List<T> getEntities();

	T getById(String id);
}
