package hu.braininghub.bh06.todo.dao;

import hu.braininghub.bh06.todo.model.TodoEntry;

public interface TodoEntryDAO extends BaseDAO<TodoEntry> {

	
}
