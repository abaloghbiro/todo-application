package hu.braininghub.bh06.todo.dao;

import hu.braininghub.bh06.todo.model.Todo;

public interface TodoDAO extends BaseDAO<Todo> {

	
}
