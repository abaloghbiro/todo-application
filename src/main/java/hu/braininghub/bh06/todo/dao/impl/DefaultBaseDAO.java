package hu.braininghub.bh06.todo.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import hu.braininghub.bh06.todo.dao.BaseDAO;
import hu.braininghub.bh06.todo.dao.exception.EntityNotFoundException;
import hu.braininghub.bh06.todo.model.BusinessObject;

public class DefaultBaseDAO<T extends BusinessObject> implements BaseDAO<T> {

	protected Map<String, T> store = new HashMap<>();

	@Override
	public void save(T object) {
		object.setId(UUID.randomUUID().toString());
		object.setActive(Boolean.TRUE);
		store.put(object.getId(), object);
	}

	@Override
	public void update(T object) {

		T stored = store.get(object.getId());

		if (stored == null) {
			throw new EntityNotFoundException(object.getId());
		}

		store.put(object.getId(), object);
	}

	@Override
	public void delete(String id) {

		T stored = store.get(id);

		if (stored == null) {
			throw new EntityNotFoundException(id);
		}
		stored.setActive(Boolean.FALSE);
	}

	@Override
	public List<T> getEntities() {
		return Collections.unmodifiableList(new ArrayList<>(store.values()));
	}

	@Override
	public T getById(String id) {

		T stored = store.get(id);

		if (stored == null) {
			throw new EntityNotFoundException(id);
		}
		return stored;
	}

}
