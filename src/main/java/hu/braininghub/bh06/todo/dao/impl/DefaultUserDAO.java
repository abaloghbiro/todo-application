package hu.braininghub.bh06.todo.dao.impl;

import java.util.List;
import java.util.stream.Collectors;

import hu.braininghub.bh06.todo.dao.UserDAO;
import hu.braininghub.bh06.todo.dao.exception.EntityNotFoundException;
import hu.braininghub.bh06.todo.model.User;

public class DefaultUserDAO extends DefaultBaseDAO<User> implements UserDAO {

	@Override
	public User getUserByName(String user) {

		List<User> users = store.values().stream().filter(u -> u.getUsername().equalsIgnoreCase(user))
				.collect(Collectors.toList());

		if (users == null || users.isEmpty()) {
			throw new EntityNotFoundException(user);
		}

		return users.get(0);
	}

}
