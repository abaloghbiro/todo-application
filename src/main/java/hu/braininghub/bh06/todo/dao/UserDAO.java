package hu.braininghub.bh06.todo.dao;

import hu.braininghub.bh06.todo.model.User;

public interface UserDAO extends BaseDAO<User> {

	User getUserByName(String user);

}
